<option>- Select -</option>
@if(!empty($collections))
    @foreach($collections as $key => $value)
        <option value="{{ $key }}">{{ $value }}</option>
    @endforeach
@endif