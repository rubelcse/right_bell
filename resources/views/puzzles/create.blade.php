@extends('layouts.app')

<style>
    #pattern-box {
        display: none;
    }
</style>

@section('content')
    <h3 class="page-title">@lang('quickadmin.puzzles.puzzles')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['puzzles.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.create')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('stage_id', 'Stage*', ['class' => 'control-label', 'id' => 'stage-id']) !!}
                    {!! Form::select('stage_id', $stages, old('stage_id'), ['class' => 'form-control']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('stage_id'))
                        <p class="help-block">
                            {{ $errors->first('stage_id') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('level', 'Level*', ['class' => 'control-label', 'id' => 'level-id']) !!}
                    {!! Form::select('level', ['' => '- Select levels -'], old('level'), ['class' => 'form-control']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('level'))
                        <p class="help-block">
                            {{ $errors->first('level') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row row-centered">
                <div class="col-xs-6 col-centered">
                    <div id="pattern-box">
                        {{--<div class="pattern-schema">
                            <div class="row row-centered">
                                <div class="col-xs-2 form-group col-centered">
                                    <input type="text" name="word_1[]" class="form-control" id="mt-1">
                                </div>
                                <div class="col-xs-2 form-group col-centered">
                                    <input type="text" name="word_1[]" class="form-control" id="mt-2">
                                </div>
                                <div class="col-xs-2 form-group col-centered">
                                    <input type="text" name="word_1[]" class="form-control" id="mt-3">
                                </div>
                            </div>
                            <div class="row row-centered">
                                <div class="col-xs-2 form-group col-centered">
                                    <input type="text" name="word_2[]" class="form-control" id="mt-4">
                                </div>
                                <div class="col-xs-2 form-group col-centered">
                                    <input type="text" name="word_2[]" class="form-control" id="mt-5">
                                </div>
                                <div class="col-xs-2 form-group col-centered">
                                    <input type="text" name="word_2[]" class="form-control" id="mt-6">
                                </div>
                            </div>
                            <div class="row row-centered">
                                <div class="col-xs-2 form-group col-centered">
                                    <input type="text" name="word_3[]" class="form-control" id="mt-7">
                                </div>
                                <div class="col-xs-2 form-group col-centered">
                                    <input type="text" name="word_3[]" class="form-control" id="mt-8">
                                </div>
                                <div class="col-xs-2 form-group col-centered">
                                    <input type="text" name="word_3[]" class="form-control" id="mt-9">
                                </div>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop
@section('javascript')
    <script>
        $(function () {
            //stage wise level load
            $("select[name='stage_id']").change(function () {
                $('#pattern-box').hide();
                var stage_id = $(this).val();
                var token = $("input[name='_token']").val();
                $.ajax({
                    url: "<?php echo route('stage-wise-level') ?>",
                    method: 'POST',
                    data: {stage_id: stage_id, _token: token},
                    success: function (data) {
                        //alert(data.options);
                        $("select[name='level']").html('');
                        $("select[name='level']").html(data.options);
                    }
                });
            });
            //stage level wise pattern schema load
            $("select[name='level']").change(function () {
                var level = $(this).val();
                var stage_id = $("select[name='stage_id']").val();
                var token = $("input[name='_token']").val();
                $.ajax({
                    url: "<?php echo route('pattern-schema') ?>",
                    method: 'POST',
                    data: {stage_id: stage_id, level: level, _token: token},
                    success: function (data) {
                        $('#pattern-box').show();
                        $("#pattern-box").html('');
                        $("#pattern-box").html(data.schema);
                    }
                });
            });
        });
    </script>
@endsection

