@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.puzzles.puzzles')</h3>

    <p>
        <a href="{{ route('puzzles.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($puzzles) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th style="text-align:center;">Sr</th>
                        <th>Stage</th>
                        <th>Level</th>
                        <th>Word-1</th>
                        <th>Word-2</th>
                        <th>Word-3</th>
                        <th>Word-4</th>
                        <th>Word-5</th>
                        <th>Word-6</th>
                        <th>Word-7</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($puzzles) > 0)
                        @foreach ($puzzles as $key => $puzzle)
                            <tr data-entry-id="{{ $puzzle->id }}">
                                <td>{{++$key}}</td>
                                <td>{{ $puzzle->stage->name or '' }}</td>
                                <td>{{ $puzzle->level->name or '' }}</td>
                                <td>{!! $puzzle->word_1 !!}</td>
                                <td>{!! $puzzle->word_2 !!}</td>
                                <td>{!! $puzzle->word_3 !!}</td>
                                <td>{!! $puzzle->word_4 !!}</td>
                                <td>{!! $puzzle->word_5 !!}</td>
                                <td>{!! $puzzle->word_6 !!}</td>
                                <td>{!! $puzzle->word_7 !!}</td>
                                {{--<td>
                                    <a href="{{ route('puzzles.show',[$question->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.view')</a>
                                    <a href="{{ route('puzzles.edit',[$question->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.edit')</a>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.are_you_sure")."');",
                                        'route' => ['puzzles.destroy', $question->id])) !!}
                                    {!! Form::submit(trans('quickadmin.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                </td>--}}
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7">@lang('quickadmin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    {{--<script>
        window.route_mass_crud_entries_destroy = '{{ route('questions.mass_destroy') }}';
    </script>--}}
@endsection