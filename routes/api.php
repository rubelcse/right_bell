<?php

Route::group(['prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'api.'], function () {
    //Manage User
   // Route::post('register', 'AuthController@appUserRegistration');
    Route::post('login', 'AuthController@appUserLogin');
    Route::post('user-details', 'AuthController@appUserDetails');
    Route::post('profile-update', 'AuthController@appUserProfileUpdate');

    //Manage Dashboard
    Route::post('top-scorer', 'GamesController@topScorer');


    //Manage Games
    Route::post('questions', 'QuestionController@getAllQuestions');
    Route::post('random_questions', 'QuestionController@getRandomQuestions');
    Route::post('read_questions', 'QuestionController@getReadQuestions');
    Route::get('result-generate', 'QuestionController@generateResult');
    Route::post('store-coin', 'GamesController@storeCoins');

    //Manage Puzzle Games
    Route::post('get-puzzle', 'PuzzleController@getAllPuzzles');
    Route::post('get-user-puzzle-summery', 'PuzzleController@getUserPuzzleSummery');

    //Manage Package category and package
    Route::post('package-category', 'PackageController@getAllCategory');
    Route::post('package-category/packages', 'PackageController@getPackageDependOnCategory');

    //Manage Transaction
    Route::post('transaction-store', 'TransactionController@store');
});
