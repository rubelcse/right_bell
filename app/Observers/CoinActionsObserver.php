<?php

namespace App\Observers;

use App\AppUser;

class CoinActionsObserver
{
    public function created($model)
    {
        //$request = \request()->all();
        //$field = $request['types'] == 1 ? 'total_quiz_point' : 'total_puzzle_point';
        $appUser = AppUser::find($model->app_user_id);
        if ($model->is_increase == 1) {
            //$appUser->{$field} += $request['points'];
            $appUser->points += $model->points;
        } else {
           // $appUser->{$field} -= $request['points'];
            $appUser->points -= $model->points;
        }
        $appUser->save();
    }
}