<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppUserPuzzleSummery extends Model
{
    protected $fillable = ['app_user_id','stage_id','level','completed'];
}
