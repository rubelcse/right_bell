<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;

    protected $fillable = ['txn_no','txn_date','app_user_id','package_category_id','package_id','status','txn_fees','amount'];

    public function appUser()
    {
        return $this->belongsTo(AppUser::class);
    }
}
