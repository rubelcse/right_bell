<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Puzzle extends Model
{
    protected $fillable = ['stage_id','level_id','word_1','word_2','word_3','word_4','word_5','word_6','word_7','status'];

    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    public function level()
    {
        return $this->belongsTo(Level::class);
    }

    public function appUsers()
    {
        return $this->belongsToMany(AppUser::class, 'app_user_puzzle', 'puzzle_id', 'app_user_id');
    }
}
