<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Topic
 *
 * @package App
 * @property string $title
*/
class Topic extends Model
{
    use SoftDeletes;

    protected $fillable = ['title','take_question_limit','status'];

    public static function boot()
    {
        parent::boot();

        Topic::observe(new \App\Observers\UserActionsObserver);
    }

    public function getTopicsWiseQuestion()
    {
        return $this->questions()->orderBy(DB::raw('RAND()'))
            ->take($this->take_question_limit);
    }

    public function questions()
    {
        return $this->hasMany(Question::class, 'topic_id');
    }

}
