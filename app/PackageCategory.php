<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageCategory extends Model
{
    use SoftDeletes;
    protected $fillable = ['name','status'];

    public function packages()
    {
        return $this->hasMany(Package::class, 'package_category_id');
    }
}
