<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppUser extends Model
{
    use SoftDeletes;
    protected $fillable = ['name','phone_no','profile_image', 'status','points','life','help','page_qty'];

    public function coins()
    {
        return $this->hasMany(Coin::class,'app_user_id');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class,'app_user_id');
    }

    public function puzzles()
    {
        return $this->belongsToMany(Puzzle::class, 'app_user_puzzle', 'app_user_id', 'puzzle_id');
    }

    public function puzzleSummery()
    {
        //return $this->hasMany()
    }
}
