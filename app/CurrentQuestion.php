<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrentQuestion extends Model
{
    protected $fillable = ['id','topic_id','question'];

    public function options()
    {
        return $this->hasMany(QuestionsOption::class, 'question_id','id');
    }
}
