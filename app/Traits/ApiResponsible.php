<?php
/**
 * Created by PhpStorm.
 * User: mdrub
 * Date: 4/6/2018
 * Time: 12:22 PM
 */

namespace App\Traits;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;

trait ApiResponsible
{
    private function successResponse($data, $code)
    {
        return response()->json($data, $code);
    }

    protected function errorResponse($message, $code)
    {
        return response()->json(['errors' => $message, 'code' => $code],$code);
    }

    protected  function showAll(Collection $collection, $code=200)
    {
        if ($collection->isEmpty()){
            return $this->successResponse(['result' => $collection],$code);
        }
        if (request('per_page')){
            $collection = $this->paginate($collection);
        }
       // dd(request('per_page'));
        return $this->successResponse(['result' => $collection],$code);
    }

    protected function showOne(Model $instance, $code = 200)
    {
        return $this->successResponse(['result' => $instance],$code);
    }

    protected function pagination(LengthAwarePaginator $data, $code = 200)
    {
        return $this->successResponse(['result' => $data],$code);
    }


    protected function paginate(Collection $collection)
    {
        $page = LengthAwarePaginator::resolveCurrentPage();
        $perPage = 15;
        if (request()->has('per_page')){
            $perPage =(int) request()->per_page;
        }
        $result = $collection->slice(($page - 1) * $perPage, $perPage)->values();
        $paginated = new LengthAwarePaginator($result,$collection->count(),$perPage,$page,[
            'path' => LengthAwarePaginator::resolveCurrentPath(),
        ]);
        $paginated->appends(request()->all());   //this append access used to other filter support this pagination
        return $paginated;
    }

    public function showMessage($message, $code = 200)
    {
        return $this->successResponse(['result' => $message],$code);
    }

    public function dataWithMessage($data, $message, $code = 200)
    {
        return $this->successResponse(['result' => $data, 'message'=> $message], $code);
    }

}