<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    protected $fillable = ['name','row','column','no_of_level','no_of_pattern','created_by','updated_by'];

    public function levels()
    {
        return $this->hasMany(Level::class, 'stage_id');
    }

    public function patterns()
    {
        return $this->hasMany(Pattern::class, 'stage_id');
    }

    public function puzzles()
    {
        return $this->hasMany(Puzzle::class,'stage_id');
    }
}
