<?php

namespace App\Http\Controllers;

use App\Pattern;
use App\Stage;
use Illuminate\Http\Request;

class PatternSchemaController extends Controller
{
    public function patternSchema(Request $request)
    {
        if ($request->ajax()) {
            $stage_id = $request->stage_id;
            $level = $request->level;
            $stage = Stage::withCount('patterns')->whereId($stage_id)->first();
            $total_pattern = $stage->patterns_count;
            //select pattern id
            $level = ($level <= $total_pattern) ? $level : ($level - $total_pattern);
            //dump($level);
            //dump($stage_id);
            //dump($level);
            $pattern = Pattern::where(['stage_id' => $stage_id, 'level' => $level])->first();
            //dd($pattern);
            $data = view('puzzles.pattern_schema',compact('stage','pattern'))->render();
            return response()->json(['schema'=>$data]);
            /*dump($pattern);
            dump($total_pattern);
            dd($pattern_id);*/

        }
    }
}
