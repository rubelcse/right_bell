<?php

namespace App\Http\Controllers;

use App\Level;
use Illuminate\Http\Request;

class ManageDropDownController extends Controller
{
    public function levelListDependOnStage(Request $request)
    {
        if($request->ajax()){
            $collections = Level::where('stage_id',$request->stage_id)->pluck("name","level")->all();
            $data = view('ajax-select',compact('collections'))->render();
            return response()->json(['options'=>$data]);
        }
    }
}
