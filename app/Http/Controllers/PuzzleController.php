<?php

namespace App\Http\Controllers;

use App\Level;
use App\Puzzle;
use App\Stage;
use Illuminate\Http\Request;

class PuzzleController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $puzzles = Puzzle::with('stage','level')->orderBy('id', 'asc')->get();
        return view('puzzles.index', compact('puzzles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stages = Stage::get()->pluck('name', 'id')->prepend('- select stage -', '');
        return view('puzzles.create', compact('stages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Puzzle  $puzzle
     * @return \Illuminate\Http\Response
     */
    public function show(Puzzle $puzzle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Puzzle  $puzzle
     * @return \Illuminate\Http\Response
     */
    public function edit(Puzzle $puzzle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Puzzle  $puzzle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Puzzle $puzzle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Puzzle  $puzzle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Puzzle $puzzle)
    {
        //
    }
}
