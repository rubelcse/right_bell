<?php

namespace App\Http\Controllers\Api\V1;

use App\AppUser;
use App\AppUserPuzzleSummery;
use App\Coin;
use App\Http\Requests\Api\StoreCoinsRequest;
use App\Http\Requests\TopScorerRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class GamesController extends ApiController
{
    public function storeCoins(StoreCoinsRequest $request)
    {
        $appUserId = $request->app_user_id;
        $user = AppUser::find($appUserId);
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token) || empty($user)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        $coin = $user->coins()->create($request->except('app_user_id'));
        if ($request->has('is_complted') && $request->is_complted == 2) { //1 = complete; 2 = not complete
            $userPuzzle = AppUserPuzzleSummery::where(['app_user_id' => $appUserId, 'stage_id' => $request->stage_id, 'level' => $request->level])->first();
            if (!empty($userPuzzle)){
                //update puzzle summery
                $userPuzzle->completed += 1;
                $userPuzzle->save();
            } else {
                //create puzzle summery
                AppUserPuzzleSummery::create([
                    'app_user_id' => $appUserId,
                    'stage_id' => $request->stage_id,
                    'level' => $request->level,
                    'completed' => 1,
                ]);
            }
        }
        $appUserInfo = $coin->appUser;
        return response()->json($appUserInfo, 200);
    }

    public function topScorer(TopScorerRequest $request)
    {
        $user_id = $request->app_user_id;
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        $allScorers = DB::table('app_users as u')
            ->join(DB::raw('(SELECT app_user_id, SUM(points) as score FROM coins c 
                where is_increase=1 AND MONTH(c.created_at) = MONTH(CURRENT_DATE())
                AND YEAR(c.created_at) = YEAR(CURRENT_DATE()) GROUP BY app_user_id ORDER BY SUM(points)) as ts'), function ($join) {
            $join->on('ts.app_user_id', '=', 'u.id');
        })
            ->select(['u.id', 'u.name', 'u.profile_image', 'ts.score']) //Select fields you need.
            ->orderBy('ts.score', 'desc')
            ->get();

        $position = $allScorers->search(function($score) use ($user_id){
            return $score->id == $user_id;
        });

        $own_position = $allScorers->where('id',$user_id)->first();
        $own_position->position = $position + 1;
        return response()->json(['top_scorer' => $allScorers->take(10), 'own_info' => $own_position], 200);
    }
}
