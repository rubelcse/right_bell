<?php

namespace App\Http\Controllers\Api\V1;

use App\AppUser;
use App\CurrentQuestion;
use App\Http\Requests\QuestionGetRequest;
use App\Question;
use App\QuestionsOption;
use App\Topic;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class QuestionController extends ApiController
{
    public function getAllQuestions(QuestionGetRequest $request)
    {
        $user = AppUser::find($request->app_user_id);
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token) || empty($user)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        $checkTodayQuestion = CurrentQuestion::whereDate('created_at', '=', date('Y-m-d'))->first();
        if (empty($checkTodayQuestion)) {
            //first delete all previouse data
            CurrentQuestion::truncate();
            //insert data
            $topics = Topic::whereStatus(1)->get();
            foreach ($topics as $topic) {
                $questions = $topic->getTopicsWiseQuestion->map->only(['id', 'topic_id', 'question']);
                foreach ($questions as $question) {
                    $data['id'] = $question['id'];
                    $data['topic_id'] = $question['topic_id'];
                    $data['question'] = $question['question'];
                    $data['created_at'] = Carbon::now();
                    $data['updated_at'] = Carbon::now();
                    CurrentQuestion::create($data);
                }
            }
        }
        //return question with options
        $questions = CurrentQuestion::with(['options' => function ($q) {
            $q->select('id', 'question_id', 'option', 'answer');
        }])->orderBy('id')->take($request->take)->get(['id', 'question']);
        if (($questions->count()) > 0 && ($request->is_decrease == 1)) {
            $user->page_qty -= 1;
            $user->save();
        }
        return response()->json([
            'total_pages' => $user->page_qty,
            'questions' => $questions
        ], 200);

    }

    public function generateResult()
    {
        $options = QuestionsOption::all();
        foreach ($options as $option) {
            if ($option->id % 2 == 0) {
                $option->answer = 0;
                $option->save();
            }
        }
        echo 'success';
    }

    public function getRandomQuestions(Request $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        $questions = Question::with(['options' => function ($q) {
            $q->select('id', 'question_id', 'option', 'answer');
        }])->orderBy(DB::raw('RAND()'))->take($request->take)->get(['id', 'question']);
        return response()->json($questions, 200);
    }

    public function getReadQuestions(Request $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        $per_page = $request->has('per_page') ? $request->per_page : '15';
        $questions = Question::select('id', 'question')->with(['correctOptions' => function ($query) {
            $query->select('question_id', 'option');
        }])->paginate($per_page);
        return response()->json($questions, 200);
    }
}
