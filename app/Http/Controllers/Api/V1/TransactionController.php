<?php

namespace App\Http\Controllers\Api\V1;

use App\AppUser;
use App\Http\Requests\TransactionRequest;
use App\Package;
use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class TransactionController extends Controller
{
    public function store(TransactionRequest $request)
    {
        $user = AppUser::find($request->app_user_id);
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token) || empty($user)) {
            return response()->json(['message' => 'Not Authenticate']);
        }

        $category_id = $request->package_category_id;
        $package = Package::where(['package_category_id' => $category_id, 'id' => $request->package_id])->first();
        if ($category_id == 1) {
            $update_field = 'help';
        } elseif ($category_id == 2) {
            $update_field = 'page_qty';
        } elseif ($category_id == 3) {
            $update_field = 'points';
        } else {
            $update_field = 'help';
        }
        //$update_field = ($category_id == 1 ? 'help' : $category_id == 2 ? 'page_qty' : $category_id == 3 ? 'points' : 'help');
        $transaction = Transaction::create($request->all());
        $transaction->appUser->{$update_field} += $package->quantity;
        $transaction->appUser->save();
        return response()->json($transaction, 200);
    }
}
