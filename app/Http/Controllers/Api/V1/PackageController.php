<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\PackagesRequest;
use App\Package;
use App\PackageCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class PackageController extends ApiController
{
    public function getAllCategory(Request $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }

        $packageCategory = PackageCategory::whereStatus(1)->get(['id', 'name']);
        return response()->json($packageCategory, 200);
    }

    public function getPackageDependOnCategory(PackagesRequest $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }

        $packages = Package::where(['package_category_id' => $request->category_id, 'status' => 1])->get(['id','name', 'quantity', 'price']);
        return response()->json($packages, 200);
    }


}
