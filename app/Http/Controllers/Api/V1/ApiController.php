<?php

namespace App\Http\Controllers\Api\V1;

use App\Traits\ApiResponsible;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    use ApiResponsible;
}
