<?php

namespace App\Http\Controllers\Api\V1;

use App\AppUser;
use App\Http\Requests\Api\AppUserLoginRequest;
use App\Http\Requests\Api\AppUserRegistrationRequest;
use App\Http\Requests\UserProfileUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class AuthController extends ApiController
{
    public function appUserRegistration(AppUserRegistrationRequest $request)
    {
        $user = AppUser::create($request->all());
        return response()->json($user, 201);
    }

    public function appUserLogin(AppUserLoginRequest $request)
    {
        $user = AppUser::where('phone_no', $request->phone_no)->first();
        if (empty($user)) {
            $data = $request->all();
            $data['status'] = 1;
            $data['total_quiz_point'] = 0;
            $data['total_puzzle_point'] = 0;
            $data['life'] = 5;
            $data['page_qty'] = 5;
            $user = AppUser::create($data);
            $user->coins()->create([
                'types' => 3,
                'is_increase' => 1,
                'points' => 100
            ]);
        } else {
            $user->name = $request->name;
            if ($user->isDirty()) {
                $user->save();
            }
        }
        $userInfo = AppUser::find($user->id);
        return response()->json($userInfo, 200);
    }

    public function appUserDetails(Request $request)
    {
        if(!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message'=>'Not Authenticate']);
        }
        $user = AppUser::find($request->id);
        //$user->profile_image = public_path() . '/' . $user->profile_image;
        if (!empty($user->profile_image)) {
            $user->profile_image = 'http://192.168.10.101:800/right_bell//public' . '/' . $user->profile_image;
        }
        return response()->json($user, 200);
    }

    public function appUserProfileUpdate(UserProfileUpdateRequest $request)
    {
        $appUser = AppUser::find($request->app_user_id);
        if ($request->has('profile_image')) {
            if (!empty($appUser->profile_image)) {
                $image_path =public_path() . '/' . $appUser->profile_image;
                if(File::exists($image_path)) {
                    File::delete($image_path);
                }
            }
            $image = $request->profile_image;  // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = $appUser->id.'_'.str_random(10).'.'.'png';
            Image::make($image)->resize(60, 60)->save(public_path('uploads/users') . '/' . $imageName);
            //user data update
            $appUser->profile_image = 'uploads/users/'.$imageName;
        }
        $appUser->name = $request->name;
        $appUser->save();
        $appUser->profile_image = public_path() . '/' .  $appUser->profile_image;
        return response()->json($appUser, 200);
    }
}
