<?php

use Illuminate\Database\Seeder;

class PackageCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\PackageCategory::class, 4)->create()->each(function($category) {
            $category->packages()->saveMany(factory(\App\Package::class,5)->make());
        });
    }
}
