<?php

use App\Level;
use Illuminate\Database\Seeder;

class LevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $collections = [
            /*Stage-1 = 15 level*/
            ['stage_id' => 1,'level' => 1, 'name' => 'Level-1 by stage 1','no_of_moves' => 40, 'status' => 1],
            ['stage_id' => 1,'level' => 2, 'name' => 'Level-2 by stage 1','no_of_moves' => 45, 'status' => 1],
            ['stage_id' => 1,'level' => 3, 'name' => 'Level-3 by stage 1','no_of_moves' => 45, 'status' => 1],
            ['stage_id' => 1,'level' => 4, 'name' => 'Level-4 by stage 1','no_of_moves' => 45, 'status' => 1],
            ['stage_id' => 1,'level' => 5, 'name' => 'Level-5 by stage 1','no_of_moves' => 45, 'status' => 1],
            ['stage_id' => 1,'level' => 6, 'name' => 'Level-6 by stage 1','no_of_moves' => 45, 'status' => 1],
            ['stage_id' => 1,'level' => 7, 'name' => 'Level-7 by stage 1','no_of_moves' => 45, 'status' => 1],
            ['stage_id' => 1,'level' => 8, 'name' => 'Level-8 by stage 1','no_of_moves' => 45, 'status' => 1],
            ['stage_id' => 1,'level' => 9, 'name' => 'Level-9 by stage 1','no_of_moves' => 45, 'status' => 1],
            ['stage_id' => 1,'level' => 10, 'name' => 'Level-10 by stage 1','no_of_moves' => 45, 'status' => 1],
            ['stage_id' => 1,'level' => 11, 'name' => 'Level-11 by stage 1','no_of_moves' => 45, 'status' => 1],
            ['stage_id' => 1,'level' => 12, 'name' => 'Level-12 by stage 1','no_of_moves' => 45, 'status' => 1],
            ['stage_id' => 1,'level' => 13, 'name' => 'Level-14 by stage 1','no_of_moves' => 45, 'status' => 1],
            ['stage_id' => 1,'level' => 14, 'name' => 'Level-14 by stage 1','no_of_moves' => 45, 'status' => 1],
            ['stage_id' => 1,'level' => 15, 'name' => 'Level-15 by stage 1','no_of_moves' => 45, 'status' => 1],

            /*Stage-2 = 15 level*/
            ['stage_id' => 2,'level' => 1, 'name' => 'Level-1 by stage 2','no_of_moves' => 50, 'status' => 1],
            ['stage_id' => 2,'level' => 2, 'name' => 'Level-2 by stage 2','no_of_moves' => 50, 'status' => 1],
            ['stage_id' => 2,'level' => 3, 'name' => 'Level-3 by stage 2','no_of_moves' => 50, 'status' => 1],
            ['stage_id' => 2,'level' => 4, 'name' => 'Level-4 by stage 2','no_of_moves' => 50, 'status' => 1],
            ['stage_id' => 2,'level' => 5, 'name' => 'Level-5 by stage 2','no_of_moves' => 50, 'status' => 1],
            ['stage_id' => 2,'level' => 6, 'name' => 'Level-6 by stage 2','no_of_moves' => 50, 'status' => 1],
            ['stage_id' => 2,'level' => 7, 'name' => 'Level-7 by stage 2','no_of_moves' => 50, 'status' => 1],
            ['stage_id' => 2,'level' => 8, 'name' => 'Level-8 by stage 2','no_of_moves' => 50, 'status' => 1],
            ['stage_id' => 2,'level' => 9, 'name' => 'Level-9 by stage 2','no_of_moves' => 50, 'status' => 1],
            ['stage_id' => 2,'level' => 10, 'name' => 'Level-10 by stage 2','no_of_moves' => 50, 'status' => 1],
            ['stage_id' => 2,'level' => 11, 'name' => 'Level-11 by stage 2','no_of_moves' => 50, 'status' => 1],
            ['stage_id' => 2,'level' => 12, 'name' => 'Level-12 by stage 2','no_of_moves' => 50, 'status' => 1],
            ['stage_id' => 2,'level' => 13, 'name' => 'Level-14 by stage 2','no_of_moves' => 50, 'status' => 1],
            ['stage_id' => 2,'level' => 14, 'name' => 'Level-14 by stage 2','no_of_moves' => 50, 'status' => 1],
            ['stage_id' => 2,'level' => 15, 'name' => 'Level-15 by stage 2','no_of_moves' => 50, 'status' => 1],

            /*Stage-3 = 15 level*/
            ['stage_id' => 3,'level' => 1, 'name' => 'Level-1 by stage 3','no_of_moves' => 55, 'status' => 1],
            ['stage_id' => 3,'level' => 2, 'name' => 'Level-2 by stage 3','no_of_moves' => 55, 'status' => 1],
            ['stage_id' => 3,'level' => 3, 'name' => 'Level-3 by stage 3','no_of_moves' => 55, 'status' => 1],
            ['stage_id' => 3,'level' => 4, 'name' => 'Level-4 by stage 3','no_of_moves' => 55, 'status' => 1],
            ['stage_id' => 3,'level' => 5, 'name' => 'Level-5 by stage 3','no_of_moves' => 55, 'status' => 1],
            ['stage_id' => 3,'level' => 6, 'name' => 'Level-6 by stage 3','no_of_moves' => 55, 'status' => 1],
            ['stage_id' => 3,'level' => 7, 'name' => 'Level-7 by stage 3','no_of_moves' => 55, 'status' => 1],
            ['stage_id' => 3,'level' => 8, 'name' => 'Level-8 by stage 3','no_of_moves' => 55, 'status' => 1],
            ['stage_id' => 3,'level' => 9, 'name' => 'Level-9 by stage 3','no_of_moves' => 55, 'status' => 1],
            ['stage_id' => 3,'level' => 10, 'name' => 'Level-10 by stage 3','no_of_moves' => 55, 'status' => 1],
            ['stage_id' => 3,'level' => 11, 'name' => 'Level-11 by stage 3','no_of_moves' => 55, 'status' => 1],
            ['stage_id' => 3,'level' => 12, 'name' => 'Level-12 by stage 3','no_of_moves' => 55, 'status' => 1],
            ['stage_id' => 3,'level' => 13, 'name' => 'Level-14 by stage 3','no_of_moves' => 55, 'status' => 1],
            ['stage_id' => 3,'level' => 14, 'name' => 'Level-14 by stage 3','no_of_moves' => 55, 'status' => 1],
            ['stage_id' => 3,'level' => 15, 'name' => 'Level-15 by stage 3','no_of_moves' => 55, 'status' => 1],

            /*Stage-4 = 15 level*/
            ['stage_id' => 4,'level' => 1, 'name' => 'Level-1 by stage 4','no_of_moves' => 60, 'status' => 1],
            ['stage_id' => 4,'level' => 2, 'name' => 'Level-2 by stage 4','no_of_moves' => 60, 'status' => 1],
            ['stage_id' => 4,'level' => 3, 'name' => 'Level-3 by stage 4','no_of_moves' => 60, 'status' => 1],
            ['stage_id' => 4,'level' => 4, 'name' => 'Level-4 by stage 4','no_of_moves' => 60, 'status' => 1],
            ['stage_id' => 4,'level' => 5, 'name' => 'Level-5 by stage 4','no_of_moves' => 60, 'status' => 1],
            ['stage_id' => 4,'level' => 6, 'name' => 'Level-6 by stage 4','no_of_moves' => 60, 'status' => 1],
            ['stage_id' => 4,'level' => 7, 'name' => 'Level-7 by stage 4','no_of_moves' => 60, 'status' => 1],
            ['stage_id' => 4,'level' => 8, 'name' => 'Level-8 by stage 4','no_of_moves' => 60, 'status' => 1],
            ['stage_id' => 4,'level' => 9, 'name' => 'Level-9 by stage 4','no_of_moves' => 60, 'status' => 1],
            ['stage_id' => 4,'level' => 10, 'name' => 'Level-10 by stage 4','no_of_moves' => 60, 'status' => 1],
            ['stage_id' => 4,'level' => 11, 'name' => 'Level-11 by stage 4','no_of_moves' => 60, 'status' => 1],
            ['stage_id' => 4,'level' => 12, 'name' => 'Level-12 by stage 4','no_of_moves' => 60, 'status' => 1]
        ];

       Level::insert($collections);
    }
}
