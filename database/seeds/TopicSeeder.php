<?php

use Illuminate\Database\Seeder;

class TopicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Topic::class, 5)->create()->each(function($topic) {
            $topic->questions()->saveMany(factory(App\Question::class,50)->make())->each(function ($question){
                $question->options()->saveMany(factory(\App\QuestionsOption::class,2)->make());
            });
        });
    }
}
