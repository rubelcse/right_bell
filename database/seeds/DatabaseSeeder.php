<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        Model::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call(RoleSeed::class);
        $this->call(UserSeed::class);
        $this->call(TopicSeeder::class);
        $this->call(StageSeeder::class);
        $this->call(PatternSeeder::class);
        $this->call(LevelSeeder::class);
        $this->call(PuzzlesSeeder::class);
        $this->call(PackageCategorySeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Model::reguard();
    }
}
