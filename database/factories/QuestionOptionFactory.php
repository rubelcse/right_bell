<?php

use Faker\Generator as Faker;

$factory->define(\App\QuestionsOption::class, function (Faker $faker) {
    $questionOptions = \App\QuestionsOption::get();
    $status = ($questionOptions->count() % 2 == 0) ? 1 : 0;
    return [
        'option' => $faker->word(),
        'answer' => $status
    ];
});
