<?php

use Faker\Generator as Faker;

$factory->define(App\Package::class, function (Faker $faker) {
    return [
        'name' => $faker->word(),
        'quantity' => $faker->numberBetween(5,15),
        'price' => $faker->numberBetween(200,500)
    ];
});
