<?php

use App\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    return [
        'question' => $faker->unique()->paragraph(1),
        'status' => 1,
    ];
});
