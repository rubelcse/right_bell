<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('levels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stage_id')->unsigned()->nullable();
            $table->foreign('stage_id')->references('id')->on('stages');
            $table->integer('level');
            $table->string('name');
            $table->integer('no_of_moves')->nullable();
            $table->tinyInteger('status')->default(1)->comment='1=active, 2=inactive';
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('levels');
    }
}
